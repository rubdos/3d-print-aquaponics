bottom=80;
top=100;
height=100;
wall=3;
waterlevel=70;
tube_inner_diameter=20;
tube_outer_diameter=22;
gen_support=true;
stroke=0.4;
inlet_angle=71;
inlet_height=100-wall;

// ---

$fn=120;

tube_radius=tube_inner_diameter/2;
tube_area=PI*pow(tube_radius,2);
tube_side=sqrt(tube_area);
outlet_r=(waterlevel-tube_inner_diameter-wall)*(top/2-bottom/2)/height+bottom/2;
inlet_r=(inlet_height-tube_inner_diameter-wall)*(top/2-bottom/2)/height+bottom/2;

/*skew takes an array of six angles:
 *x along y
 *x along z
 *y along x
 *y along z
 *z along x
 *z along y
 */
module skew(dims) {
matrix = [
	[ 1, tan(dims[0]), tan(dims[1]), 0 ],
	[ tan(dims[2]), 1, tan(dims[3]), 0 ],
	[ tan(dims[4]), tan(dims[5]), 1, 0 ],
	[ 0, 0, 0, 1 ]
];
multmatrix(matrix)
children();
}

module bucket() {
    difference() {
        cylinder(r1=bottom/2, r2=top/2, h=height);
        translate([0, 0, wall]) cylinder(r1=bottom/2-wall, r2=top/2-wall, h=height-wall+0.1);
        // Cut out the outlet.
        translate([outlet_r, 0, waterlevel-tube_radius])
        rotate([0, 90, 0])
            cylinder(r=tube_radius, h=10*wall, center=true);
        // Inlet with bars
        rotate([0, 0, inlet_angle])
        translate([inlet_r, 0, inlet_height-tube_radius])
        rotate([0, 90, 0]) difference() {
            cylinder(r=tube_radius, h=10*wall, center=true);
            for(i=[-tube_radius:4:tube_radius]) {
                translate([0, i, 0]) cube(size=[tube_radius*2, 1, 10*wall], center=true);
            }
        }
    }
}

module angled_connector(support_height=0) {
    difference() {
        union() {
            difference() {
                intersection() {
                    rotate([0, 90, 0])
                    cylinder(r=tube_radius+wall, h=tube_inner_diameter+wall*2);
                    translate([tube_radius+wall, 0, -tube_radius-wall])
                    cylinder(r=tube_radius+wall, h=tube_inner_diameter+wall*2);
                }
                intersection() {
                    rotate([0, 90, 0])
                    cylinder(r=tube_radius, h=tube_inner_diameter);
                    translate([tube_radius+wall, 0, -tube_radius])
                    cylinder(r=tube_radius, h=tube_inner_diameter);
                }
                rotate([0, 90, 0])
                cylinder(r=tube_radius, h=tube_radius+wall);
            }
            rotate([0, 90, 0]) translate([0, 0, -wall])
            cylinder(r=tube_radius+wall, h=tube_radius+wall*2);
            translate([tube_radius+wall, 0, -tube_radius-wall])
            cylinder(r=tube_radius+wall, h=tube_radius+wall);
         }
         rotate([0, 90, 0]) translate([0, 0, -wall])
         cylinder(r=tube_radius, h=tube_radius+wall*2);
         translate([tube_radius+wall, 0, -tube_radius-wall])
         cylinder(r=tube_radius, h=tube_radius+wall);
    }
    // connector for tube
    tube_connector_height=tube_outer_diameter;
    translate([tube_radius+wall, 0, -tube_radius-wall]) {
        rotate([180, 0, 0])
        difference() {
            cylinder(r1=tube_radius+wall, r2=tube_radius, h=tube_connector_height);
            cylinder(r1=tube_radius, r2=tube_radius-wall, h=tube_connector_height);
        }
    }
    // Generate support
    #
    if(support_height > 0 && gen_support) {
        delta_support_height=support_height-tube_connector_height-tube_radius-wall;
        translate([tube_radius+wall, 0, -tube_radius-wall-tube_connector_height])
        rotate([180, 0, 0]) {
            difference() {
                cylinder(h=delta_support_height,r=tube_radius-wall+stroke/2);
                cylinder(h=delta_support_height,r=tube_radius-wall-stroke/2);
            }
            difference() {
                cylinder(h=delta_support_height,r=tube_radius+stroke/2);
                cylinder(h=delta_support_height,r=tube_radius-stroke/2);
            }
        }
        translate([tube_radius+wall, 0, -support_height]) {
            cylinder(h=1,r=tube_radius+stroke/2);
        }
    }
}

module bell() {
    // The bell syphon is
    intersection() {
        // the inside part of the bucket
        cylinder(r1=bottom/2, r2=top/2, h=height);
        translate([bottom/2, 0, 0])
        skew([0, atan2(top/2-bottom/2,height), 0, 0, 0, 0]) {
            // of the skew cylinder
            difference() {
                cylinder(r=tube_radius+wall, h=waterlevel+wall);
                // hollowed out
                cylinder(r=tube_radius, h=waterlevel);
                difference() {
                    // and cut at the bottom. XXX: tube_side is an estimate.
                    // The real height should be calculated from the area on the cylinder.
                    cylinder(r=tube_radius+wall, h=tube_side/2);
                    for(i=[0:30:180]) rotate([0, 0, i]) {
                        translate([-1, 0, 0])
                        cube([2, tube_radius+wall, 10]);
                    }
                }
            }
        }
    }
    difference() {
        union() {
            // Now, the outside of the bell syphon
            translate([outlet_r, 0, waterlevel-tube_radius])
            angled_connector(waterlevel-tube_radius);
            // Inlet connector
            rotate([0, 0, inlet_angle])
            translate([inlet_r, 0, inlet_height-tube_radius])
                angled_connector(inlet_height-tube_radius);
        }
        // Cut off the stray material of the connectors.
        translate([0, 0, wall]) cylinder(r1=bottom/2-wall, r2=top/2-wall, h=height-wall+0.1);

    }
}

bucket();
bell();